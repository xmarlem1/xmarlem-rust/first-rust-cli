

# RUST CLI


Tutorial from here: https://rust-cli.github.io/book/tutorial/index.html


```rust
$ grrs foobar test.txt
```
We expect our program to look at test.txt and print out the lines that contain foobar.
